<?php
/**
* @Entity @Table(name="orm.instrument")
**/
class Instrument
{
/**
* @Id @Column(type="string", length= 5)
**/
private $ref;

/**
* @Column(type="string", length=30) 
**/
private $nom;

/**
* @Column(type="string", length=20) 
**/
private $marque;

/**
* @Column(type="string", length=150) 
**/
private $caracteristiq;

/**
* @Column(type="integer") 
**/
private $prix;

/**
* @Column(type="string", length=20) 
**/
private $photo;

/**
* @ManyToOne(targetEntity="Categorie")
* @JoinColumn (name="la_Categorie", referencedColumnName="code_categorie")
**/
private $la_Categorie;


// constructeur par défaut
public function __instrument()
{
$this->ref = 0;
 $this->nom = "categorie_defaut";
 $this->marque = "marque_defaut";
 $this->caracteristiq = "caracteristiq_defaut";
 $this->prix = 0;
 $this->photo = "photo_defaut";
 $this->la_Categorie = null;
 
}

// constructeur avec paramètres pour initialiser les propriétés de Region

    public function init($ref, $nom, $marque, $caracteristiq, $prix, $photo, $categorie) {
        $this->ref = $ref;
        $this->nom = $nom;
        $this->marque = $marque;
        $this->caracteristiq = $caracteristiq;
        $this->prix = $prix;
        $this->photo = $photo;
        $this->la_Categorie = $categorie;
    }
    
    function getRef() {
        return $this->ref;
    }

    function getNom() {
        return $this->nom;
    }

    function getMarque() {
        return $this->marque;
    }

    function getCaracteristiq() {
        return $this->caracteristiq;
    }

    function getPrix() {
        return $this->prix;
    }

    function getPhoto() {
        return $this->photo;
    }

    function getCategorie() {
        return $this->la_Categorie;
    }

    function setRef($ref) {
        $this->ref = $ref;
    }

    function setNom($nom) {
        $this->nom = $nom;
    }

    function setMarque($marque) {
        $this->marque = $marque;
    }

    function setCaracteristiq($caracteristiq) {
        $this->caracteristiq = $caracteristiq;
    }

    function setPrix($prix) {
        $this->prix = $prix;
    }

    function setPhoto($photo) {
        $this->photo = $photo;
    }

    function setCategorie($categorie) {
        $this->la_Categorie = $categorie;
    }



}

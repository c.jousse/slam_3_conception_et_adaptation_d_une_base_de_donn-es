<?php
/**
* @Entity @Table(name="orm.categorie")
**/
class Categorie
{
/**
* @Id @Column(length=2)
**/
private $code_categorie;

/**
* @Column(length=30) 
**/
private $nom_categorie;


// *** puis, constructeurs ainsi que méthodes get et set éventuelles
// constructeur par défaut
public function __categorie()
{
$this->code_categorie = 0;
$this->nom_categorie = "categorie_defaut";
}

// constructeur avec paramètres pour initialiser les propriétés de Region
public function init($code_categorie, $nom_categorie)
{
$this->code_categorie = $code_categorie;
$this->nom_categorie = $nom_categorie;
}
public function getCode_categorie()
{
return $this->code_categore;
}
public function getNom_categorie()
{
return $this->nom_categorie;
}

function setCode_categorie($code_categorie) {
    $this->code_categorie = $code_categorie;
}

function setNom_categorie($nom_categorie) {
    $this->nom_categorie = $nom_categorie;
}


}